# GeyserPi

A geyser utility management system based upon the Raspberry Pi.

## Details

Reduce electrical power consumption through setting a predifined heating schedule for an electrical geyser, additionally cycling pre-heated water from a solar geyser into the system.

## Hardware Requirements

1. Raspberry Pi, with Python and a LAMP or LEMP stack.
2. Two DS18B20 sealed temperature probes.
3. Relay breakout board with two relays.

## Team Members

* [Dr. Henk van Rooyen](http://www.vanrooyen.co.za)
* [Cor Cronje](http://www.phpweb.co.za)